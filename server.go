package main

import (
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/joho/godotenv"
	"gitlab.com/zadiv/library/graph"
	db "gitlab.com/zadiv/library/graph/db/rethinkdb"
	"gitlab.com/zadiv/library/graph/generated"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

const defaultPort = "8080"

func main() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	var (
		once    sync.Once
		session *r.Session
	)
	once.Do(func() {
		session = db.Init()
	})
	pm := db.NewPublisherManager(session, "publishers")
	am := db.NewAuthorManager(session, "authors")
	bm := db.NewBookManager(session, "books")

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{PM: pm, AM: am, BM: bm}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
