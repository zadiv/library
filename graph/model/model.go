package model

type Author struct {
	ID   string  `json:"id" rethinkdb:"id,omitempty"`
	Name string  `json:"name" rethinkdb:"name"`
	Bio  *string `json:"bio" rethinkdb:"bio"`
}

func (Author) IsSearchResult() {}

type BaseModel struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Book struct {
	ID          string     `json:"id" rethinkdb:"id,omitempty"`
	Name        string     `json:"name" rethinkdb:"name"`
	Slug        *string    `json:"slug" rethinkdb:"slug,omitempty"`
	Description string     `json:"description" rethinkdb:"description"`
	Edition     int        `json:"edition" rethinkdb:"edition"`
	Authors     []*Author  `json:"authors" rethinkdb:"authors"`
	AuthorIDs   []string   `json:"author_ids" rethinkdb:"author_ids"`
	PublishedAt string     `json:"publishedAt" rethinkdb:"published_at"`
	Isbn        string     `json:"isbn" rethinkdb:"isbn"`
	Page        int        `json:"page" rethinkdb:"page"`
	Language    string     `json:"language" rethinkdb:"language"`
	PublisherID string     `json:"publisher_id" rethinkdb:"publisher_id"`
	Publisher   *Publisher `json:"publisher" rethinkdb:"publisher,omitempty"`
}

func (Book) IsSearchResult() {}

type Publisher struct {
	ID       string  `json:"id" rethinkdb:"id,omitempty"`
	Name     string  `json:"name" rethinkdb:"name"`
	Founded  *string `json:"founded" rethinkdb:"founded"`
	Location string  `json:"location" rethinkdb:"location"`
}

func (Publisher) IsSearchResult() {}
