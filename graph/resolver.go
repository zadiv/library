//go:generate go run github.com/99designs/gqlgen generate
package graph

import db "gitlab.com/zadiv/library/graph/db/rethinkdb"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	PM *db.PublisherManager
	AM *db.AuthorManager
	BM *db.BookManager
}
