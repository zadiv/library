package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/library/graph/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IAuthor interface {
	Create(ctx context.Context, input *model.NewAuthor) (*model.Author, error)
	Update(ctx context.Context, id string, input *model.NewAuthor) (*model.Author, error)
	Delete(ctx context.Context, id string) error
	Bulk(ctx context.Context, input []model.NewAuthor) ([]*model.Author, error)
	All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Author, error)
	Get(ctx context.Context, filter map[string]interface{}) (*model.Author, error)
}

type AuthorManager struct {
	Session *r.Session
	TblName string
}

func NewAuthorManager(session *r.Session, tblName string) *AuthorManager {
	return &AuthorManager{Session: session, TblName: tblName}
}

func (am *AuthorManager) Create(ctx context.Context, input *model.NewAuthor) (*model.Author, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	author := model.Author{
		Name: input.Name,
		Bio:  input.Bio,
	}
	res, err := r.Table(am.TblName).Insert(author).RunWrite(am.Session)
	if err != nil {
		return nil, err
	}
	author.ID = res.GeneratedKeys[0]
	return &author, nil
}
func (am *AuthorManager) Update(ctx context.Context, id string, input *model.NewAuthor) (*model.Author, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	data := map[string]interface{}{
		"name": input.Name,
		"bio":  input.Bio,
	}
	author := model.Author{
		Name: input.Name,
		Bio:  input.Bio,
	}
	_, err := r.Table(am.TblName).Get(id).Update(data).RunWrite(am.Session)
	if err != nil {
		return nil, err
	}
	author.ID = id
	return &author, nil
}
func (am *AuthorManager) Delete(ctx context.Context, id string) error {
	_, cancel := context.WithTimeout(ctx, 250*time.Microsecond)
	defer cancel()
	_, err := r.Table(am.TblName).Get(id).Default().RunWrite(am.Session)
	if err != nil {
		return err
	}
	return nil
}
func (am *AuthorManager) Bulk(ctx context.Context, input []*model.NewAuthor) ([]*model.Author, error) {
	var data []model.Author
	for _, d := range data {
		data = append(data, model.Author{
			Name: d.Name,
			Bio:  d.Bio,
		})
	}
	var authors []*model.Author
	res, err := r.Table(am.TblName).Insert(data).Run(am.Session)
	if err != nil {
		return nil, err
	}
	var row model.Author
	for res.Next(&row) {
		authors = append(authors, &row)
	}
	defer res.Close()
	return authors, nil
}
func (am *AuthorManager) All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Author, error) {
	_, cancel := context.WithTimeout(ctx, 1000*time.Microsecond)
	defer cancel()
	var data []*model.Author
	res, err := r.Table(am.TblName).Limit(limit).Filter(filter).Run(am.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err == r.ErrEmptyResult {
		return nil, errors.New("empty result")
	}
	if err != nil {
		return nil, err
	}

	defer res.Close()

	return data, nil
}
func (am *AuthorManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Author, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Microsecond)
	defer cancel()

	var author model.Author
	res, err := r.Table(am.TblName).Filter(filter).Run(am.Session)
	if err != nil {
		return nil, err
	}
	res.One(&author)
	return &author, nil
}
