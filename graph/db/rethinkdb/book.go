package db

import (
	"context"
	"errors"
	"math"
	"time"

	"gitlab.com/zadiv/library/graph/model"
	"gitlab.com/zadiv/library/utils/text"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IBook interface {
	Create(ctx context.Context, input *model.NewBook) (*model.Book, error)
	Update(ctx context.Context, id string, input *model.NewBook) (*model.Book, error)
	Delete(ctx context.Context, id string) error
	Bulk(ctx context.Context, input []model.NewBook) ([]*model.Book, error)
	All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Book, error)
	Get(ctx context.Context, filter map[string]interface{}) (*model.Book, error)
	Publisher(ctx context.Context, filter map[string]interface{}) (*model.Book, error)
}

type BookManager struct {
	Session *r.Session
	TblName string
}

func NewBookManager(session *r.Session, tblName string) *BookManager {
	return &BookManager{Session: session, TblName: tblName}
}

func (bm *BookManager) Publisher(ctx context.Context, filter map[string]interface{}) (*model.Book, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Microsecond)
	defer cancel()

	var book model.Book
	res, err := r.Table(bm.TblName).Filter(filter).Merge(func(book r.Term) interface{} {
		return map[string]interface{}{
			"publisher": r.Table("publishers").Get(book.Field("publisher_id")),
		}
	}).Pluck([]string{"publisher"}).Run(bm.Session)
	if err != nil {
		return nil, err
	}
	res.One(&book)
	return &book, nil
}

func (bm *BookManager) Create(ctx context.Context, input *model.NewBook) (*model.Book, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	slug := text.Slugify(input.Name)
	book := model.Book{
		Name:        input.Name,
		Slug:        &slug,
		Description: input.Description,
		PublishedAt: input.PublishedAt.String(),
		Edition:     input.Edition,
		Isbn:        input.Isbn,
		AuthorIDs:   input.AuthorIDs,
		Language:    input.Language,
		Page:        input.Page,
		PublisherID: input.PublisherID,
	}
	res, err := r.Table(bm.TblName).Insert(book).RunWrite(bm.Session)
	if err != nil {
		return nil, err
	}
	book.ID = res.GeneratedKeys[0]
	return &book, nil
}
func (bm *BookManager) Update(ctx context.Context, id string, input *model.NewBook) (*model.Book, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	slug := text.Slugify(input.Name)
	data := map[string]interface{}{
		"name":         input.Name,
		"slug":         slug,
		"description":  input.Description,
		"edition":      input.Edition,
		"isbn":         input.Isbn,
		"author_ids":   input.AuthorIDs,
		"language":     input.Language,
		"page":         input.Page,
		"publisher_id": input.PublisherID,
	}
	book := model.Book{
		Name:        input.Name,
		Slug:        &slug,
		Description: input.Description,
		Edition:     input.Edition,
		Isbn:        input.Isbn,
		AuthorIDs:   input.AuthorIDs,
		Language:    input.Language,
		Page:        input.Page,
		PublisherID: input.PublisherID,
	}
	_, err := r.Table(bm.TblName).Get(id).Update(data).RunWrite(bm.Session)
	if err != nil {
		return nil, err
	}
	book.ID = id
	return &book, nil
}
func (bm *BookManager) Delete(ctx context.Context, id string) error {
	_, cancel := context.WithTimeout(ctx, 250*time.Microsecond)
	defer cancel()
	_, err := r.Table(bm.TblName).Get(id).Default().RunWrite(bm.Session)
	if err != nil {
		return err
	}
	return nil
}
func (bm *BookManager) Bulk(ctx context.Context, input []*model.NewBook) ([]*model.Book, error) {
	var data []model.Book

	for _, d := range input {
		slug := text.Slugify(d.Name)
		data = append(data, model.Book{
			Name:        d.Name,
			Slug:        &slug,
			Description: d.Description,
			Edition:     d.Edition,
			Isbn:        d.Isbn,
			AuthorIDs:   d.AuthorIDs,
			Language:    d.Language,
			Page:        d.Page,
			PublisherID: d.PublisherID,
			PublishedAt: d.PublishedAt.String(),
		})
	}
	var books []*model.Book
	res, err := r.Table(bm.TblName).Insert(data).Run(bm.Session)
	if err != nil {
		return nil, err
	}
	var row model.Book
	for res.Next(&row) {
		books = append(books, &row)
	}
	defer res.Close()
	return books, nil
}
func (bm *BookManager) All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Book, error) {
	_, cancel := context.WithTimeout(ctx, 1000*time.Microsecond)
	defer cancel()
	var data []*model.Book
	end := (*page * *limit) + 1
	start := *page
	if *page < 0 {
		start = int(math.Abs(float64(*page)))
	}
	if *page == 0 {
		start = 1
		end = *limit
	}
	res, err := r.Table(bm.TblName).Filter(filter).Slice(start, end).Merge(func(book r.Term) interface{} {
		return map[string]interface{}{
			"authors":   r.Table("authors").GetAll(r.Args(book.Field("author_ids"))).CoerceTo("array"),
			"publisher": r.Table("publishers").Get(book.Field("publisher_id")),
		}
	}).Run(bm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err == r.ErrEmptyResult {
		return nil, errors.New("empty result")
	}
	if err != nil {
		return nil, err
	}

	defer res.Close()

	return data, nil
}
func (bm *BookManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Book, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Microsecond)
	defer cancel()

	var book model.Book
	res, err := r.Table(bm.TblName).Filter(filter).Merge(func(book r.Term) interface{} {
		return map[string]interface{}{
			"authors":   r.Table("authors").GetAll(r.Args(book.Field("author_ids"))).CoerceTo("array"),
			"publisher": r.Table("publishers").Get(book.Field("publisher_id")),
		}
	}).Run(bm.Session)
	if err != nil {
		return nil, err
	}
	res.One(&book)
	return &book, nil
}
