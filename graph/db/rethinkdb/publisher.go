package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/library/graph/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IPublisher interface {
	Create(ctx context.Context, input *model.NewPublisher) (*model.Publisher, error)
	Update(ctx context.Context, id string, input *model.NewPublisher) (*model.Publisher, error)
	Delete(ctx context.Context, id string) error
	Bulk(ctx context.Context, input []model.NewPublisher) ([]*model.Publisher, error)
	All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Publisher, error)
	Get(ctx context.Context, filter map[string]interface{}) (*model.Publisher, error)
}

type PublisherManager struct {
	Session *r.Session
	TblName string
}

func NewPublisherManager(session *r.Session, tblName string) *PublisherManager {
	return &PublisherManager{Session: session, TblName: tblName}
}

func (pm *PublisherManager) Create(ctx context.Context, input *model.NewPublisher) (*model.Publisher, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	publisher := model.Publisher{
		Name:     input.Name,
		Founded:  input.Founded,
		Location: input.Location,
	}
	res, err := r.Table(pm.TblName).Insert(publisher).RunWrite(pm.Session)
	if err != nil {
		return nil, err
	}
	publisher.ID = res.GeneratedKeys[0]
	return &publisher, nil
}
func (pm *PublisherManager) Update(ctx context.Context, id string, input *model.NewPublisher) (*model.Publisher, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	data := map[string]interface{}{
		"name":     input.Name,
		"founded":  input.Founded,
		"location": input.Location,
	}
	publisher := model.Publisher{
		Name:     input.Name,
		Founded:  input.Founded,
		Location: input.Location,
	}
	_, err := r.Table(pm.TblName).Get(id).Update(data).RunWrite(pm.Session)
	if err != nil {
		return nil, err
	}
	publisher.ID = id
	return &publisher, nil
}
func (pm *PublisherManager) Delete(ctx context.Context, id string) error {
	_, cancel := context.WithTimeout(ctx, 250*time.Microsecond)
	defer cancel()
	_, err := r.Table(pm.TblName).Get(id).Default().RunWrite(pm.Session)
	if err != nil {
		return err
	}
	return nil
}
func (pm *PublisherManager) Bulk(ctx context.Context, input []*model.NewPublisher) ([]*model.Publisher, error) {
	var data []model.Publisher
	for _, d := range data {
		data = append(data, model.Publisher{
			Name:     d.Name,
			Founded:  d.Founded,
			Location: d.Location,
		})
	}
	var products []*model.Publisher
	res, err := r.Table(pm.TblName).Insert(data).Run(pm.Session)
	if err != nil {
		return nil, err
	}
	var row model.Publisher
	for res.Next(&row) {
		products = append(products, &row)
	}
	defer res.Close()
	return products, nil
}
func (pm *PublisherManager) All(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Publisher, error) {
	_, cancel := context.WithTimeout(ctx, 1000*time.Microsecond)
	defer cancel()
	var data []*model.Publisher
	res, err := r.Table(pm.TblName).Limit(limit).Filter(filter).Run(pm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err == r.ErrEmptyResult {
		return nil, errors.New("empty result")
	}
	if err != nil {
		return nil, err
	}

	defer res.Close()

	return data, nil
}
func (pm *PublisherManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Publisher, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Microsecond)
	defer cancel()

	var publisher model.Publisher
	res, err := r.Table(pm.TblName).Filter(filter).Run(pm.Session)
	if err != nil {
		return nil, err
	}
	res.One(&publisher)
	return &publisher, nil
}
