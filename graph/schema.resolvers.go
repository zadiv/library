package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/zadiv/library/graph/generated"
	"gitlab.com/zadiv/library/graph/model"
)

func (r *bookResolver) PublishedAt(ctx context.Context, obj *model.Book) (*time.Time, error) {
	publishedAt, err := time.Parse(model.LayoutISO, obj.PublishedAt)
	if err != nil {
		return nil, err
	}
	return &publishedAt, nil
}

func (r *bookResolver) Publisher(ctx context.Context, obj *model.Book) (*model.Publisher, error) {
	res, err := r.BM.Publisher(ctx, map[string]interface{}{"id": obj.ID})
	if err != nil {
		return nil, err
	}
	return res.Publisher, nil
}

func (r *mutationResolver) CreateAuthor(ctx context.Context, input model.NewAuthor) (*model.Author, error) {
	res, err := r.AM.Create(ctx, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdateAuthor(ctx context.Context, id string, input model.NewAuthor) (*model.Author, error) {
	res, err := r.AM.Update(ctx, id, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteAuthor(ctx context.Context, id string) (bool, error) {
	if err := r.AM.Delete(ctx, id); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) BulkAuthor(ctx context.Context, input []*model.NewAuthor) ([]*model.Author, error) {
	res, err := r.AM.Bulk(ctx, input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) CreateBook(ctx context.Context, input model.NewBook) (*model.Book, error) {
	res, err := r.BM.Create(ctx, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdateBook(ctx context.Context, id string, input model.NewBook) (*model.Book, error) {
	res, err := r.BM.Update(ctx, id, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteBook(ctx context.Context, id string) (bool, error) {
	if err := r.BM.Delete(ctx, id); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) BulkBook(ctx context.Context, input []*model.NewBook) ([]*model.Book, error) {
	res, err := r.BM.Bulk(ctx, input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) CreatePublisher(ctx context.Context, input model.NewPublisher) (*model.Publisher, error) {
	res, err := r.PM.Create(ctx, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdatePublisher(ctx context.Context, id string, input model.NewPublisher) (*model.Publisher, error) {
	res, err := r.PM.Update(ctx, id, &input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeletePublisher(ctx context.Context, id string) (bool, error) {
	if err := r.PM.Delete(ctx, id); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) BulkPublisher(ctx context.Context, input []*model.NewPublisher) ([]*model.Publisher, error) {
	res, err := r.PM.Bulk(ctx, input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Books(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Book, error) {
	res, err := r.BM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Book(ctx context.Context, filter map[string]interface{}) (*model.Book, error) {
	res, err := r.BM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Authors(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Author, error) {
	res, err := r.AM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Author(ctx context.Context, filter map[string]interface{}) (*model.Author, error) {
	res, err := r.AM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Publishers(ctx context.Context, filter map[string]interface{}, limit *int, page *int) ([]*model.Publisher, error) {
	res, err := r.PM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Publisher(ctx context.Context, filter map[string]interface{}) (*model.Publisher, error) {
	res, err := r.PM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Search(ctx context.Context, filter map[string]interface{}, limit *int, page *int) (model.SearchResult, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *subscriptionResolver) BookAdded(ctx context.Context, slug string) (<-chan *model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

// Book returns generated.BookResolver implementation.
func (r *Resolver) Book() generated.BookResolver { return &bookResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type bookResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
func (r *bookResolver) AuthorSet(ctx context.Context, obj *model.Book) ([]*model.Author, error) {
	panic(fmt.Errorf("not implemented"))
}
